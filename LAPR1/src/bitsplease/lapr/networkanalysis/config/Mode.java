package bitsplease.lapr.networkanalysis.config;

public enum Mode {
	INTERACTIVE, RECURSIVE
}
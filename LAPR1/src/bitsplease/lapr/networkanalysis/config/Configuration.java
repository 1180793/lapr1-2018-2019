package bitsplease.lapr.networkanalysis.config;

public class Configuration {

	public static final char ID_PREFIX = 's';
	
	public static final String FILE_NAME_SEPARATOR = "_";
	public static final String FIELD_SEPARATOR = ",";
	
	public static final int FIELDS_NODES_FILE = 5;
	public static final int FIELDS_BRANCHES_FILE = 3;
	
	public static final int FIELDS_MODE_N = 3;
	public static final int FIELDS_MODE_TK = 5;
	public static final int FIELDS_MODE_TKD = 7;
	
	public static final int FIELD_NODE_ID = 0;
	public static final int FIELD_NODE_MEDIA = 1;
	public static final int FIELD_NODE_MEDIA_TYPE = 2;
	public static final int FIELD_NODE_TYPE_LABEL = 3;
	public static final int FIELD_NODE_URL = 4;
	
	public static final int FIELD_BRANCH_FROM = 0;
	public static final int FIELD_BRANCH_TO = 1;
	
	public static final int FIELD_ARGS_MODE = 0;
	
	public static final int FIELD_ARGS_N_NODES_FILE = 1;
	public static final int FIELD_ARGS_N_BRANCHES_FILE = 2;
	
	public static final int FIELD_ARGS_TK = 1;
	public static final int FIELD_ARGS_TK_VALUE = 2;

	public static final int FIELD_ARGS_TKD = 3;
	public static final int FIELD_ARGS_TKD_VALUE = 4;
	
	public static final int FIELD_ARGS_TK_NODES_FILE = 3;
	public static final int FIELD_ARGS_TK_BRANCHES_FILE = 4;
	
	public static final int FIELD_ARGS_TKD_NODES_FILE = 5;
	public static final int FIELD_ARGS_TKD_BRANCHES_FILE = 6;
	
	public static final int MAX_NODES = 200;
	
	public static final String LOG_FOLDER = "crash-reports/";
}
package bitsplease.lapr.networkanalysis.config;

public class Messages {

    public static final String MENU_CONTINUE = "\n[Pressione qualquer tecla para continuar]";
    public static final String INVALID_POWERVALUE = "A potência deverá ser um número positivo não nulo.";
    public static final String INVALID_NETWORK_TYPE = "O ficheiro dos ramos não tem o tipo da network definida. Por favor verifique o conteúdo do ficheiro.";
    public static final String INVALID_NETWORK_TYPE_MATCH = "A estrutura do comando não corresponde ao tipo de network encontrada.";
    public static final String INVALID_DAMPING_FACTOR = "O damping factor deve ser um número compreendido entre 0 e 1.";
    private static final String ARGUMENTS_HELP = "Introduza --help para mais informações.";
    public static final String NOT_ENOUGH_ARGUMENTS = "Argumentos insuficientes. " + ARGUMENTS_HELP;
    public static final String INVALID_ARGUMENTS_USAGE = "Uso incorreto. " + ARGUMENTS_HELP;
    public static final String INVALID_ARGUMENTS_MODIFIER = "Modificador inválido. " + ARGUMENTS_HELP;
    public static final String INVALID_ARGUMENTS = "Argumentos inválidos. " + ARGUMENTS_HELP;
    public static final String INVALID_ARGUMENTS_T_POWER = "Modificador inválido. " + ARGUMENTS_HELP;
    public static final String INVALID_ARGUMENTS_T_POWERVALUE = "A potência deve ser um número inteiro positivo não nulo. "
            + ARGUMENTS_HELP;
    public static final String INVALID_FILE = "Ficheiro(s) inexistente(s). " + ARGUMENTS_HELP;
    public static final String INVALID_NODES_FILE = "Ficheiro dos nós inválido. Por favor verifique o conteúdo do ficheiro. "
            + ARGUMENTS_HELP;
    public static final String INVALID_NODE_NUMBER = "Não há nós válidos no ficheiro. Por favor verifique o seu conteúdo. "
            + ARGUMENTS_HELP;
    public static final String INVALID_BRANCHES_FILE = "Ficheiro dos ramos inválido. Por favor verifique o conteúdo do ficheiro. "
            + ARGUMENTS_HELP;
    public static final String MAX_NODES_EXCEEDED = "Limite de nós excedido. O ficheiro contém mais de "
            + Configuration.MAX_NODES + " nós. " + ARGUMENTS_HELP;

    public static String getHelpMessage() {
        String helpInformation = "Uso: java -jar <jar name>.jar [OPÇÕES...] [FICHEIROS...]\n";

        helpInformation += "Executa algumas métricas de análise de uma network relacionadas a uma network.\n\n";
        helpInformation += "Argumentos obrigatórios para opções longas são obrigatórios para opções curtas também..\n";
        helpInformation += "-n <nodes file> <branches file>\n";
        helpInformation += "	Mostra um menu para o utilizador com operações de análise de network relacionadas a uma determinada network dos ficheiros de entrada.\n";
        helpInformation += "	O ficheiro do nós deve estar relacionado à caracterização do nó.\n";
        helpInformation += "	O ficheiro dos ramos deve estar relacionado às conexões de nós.\n\n";
        helpInformation += "-t -k <power value> <nodes file> <branches file>\n";
        helpInformation += "	Calcula e guarda todas as métricas de análise da network relacionadas a uma determinada network dos ficheiros de entrada.\n";
        helpInformation += "	O ficheiro do nós deve estar relacionado à caracterização do nó.\n";
        helpInformation += "	O ficheiro dos ramos deve estar relacionado às conexões de nós.\n\n";
        helpInformation += "	O valor da potência deve ser maior que zero.\n\n";
        helpInformation += "-t -k <iteration count> -d <damping factor> <nodes file> <branches file>\n";
        helpInformation += "	Calcula e guarda todas as métricas de análise da network relacionadas a uma determinada network dos ficheiros de entrada.\n";
        helpInformation += "	O ficheiro do nós deve estar relacionado à caracterização do nó.\n";
        helpInformation += "	O ficheiro dos ramos deve estar relacionado às conexões de nós.\n\n";
        helpInformation += "	O valor das iterações deve ser maior que zero.\n";
        helpInformation += "	O valor do damping factor deve estar compreendido entre 0 e 1.";

        return helpInformation;
    }
}
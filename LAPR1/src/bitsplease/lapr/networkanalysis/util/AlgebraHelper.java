package bitsplease.lapr.networkanalysis.util;

import org.la4j.Matrix;

public class AlgebraHelper {

	/**
	 * Multiplies two matrices.
	 *
	 * @param firstmatrix  - any matrix.
	 * @param secondmatrix - any other matrix.
	 * @return returns the multiplied matrix.
	 */
	public static double[][] multiply(double[][] firstmatrix, double[][] secondmatrix) {
		double[][] matrix = new double[firstmatrix.length][secondmatrix[0].length];

		for (int i = 0; i < firstmatrix.length; i++) {
			for (int j = 0; j < secondmatrix[0].length; j++) {
				double result = 0;

				for (int f = 0; f < firstmatrix[0].length; f++) {
					result += firstmatrix[i][f] * secondmatrix[f][j];
				}

				matrix[i][j] = result;
			}
		}

		return matrix;
	}

	/**
	 * Multiplies a matrix by a constant.
	 *
	 * @param matrix   - any matrix.
	 * @param constant - any constant.
	 * @return returns the multiplied matrix.
	 */
	public static double[][] multiply(double[][] matrix, double constant) {
		double[][] result = new double[matrix.length][matrix[0].length];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result[i][j] = matrix[i][j] * constant;
			}
		}

		return result;
	}

	/**
	 * Powers a matrix.
	 *
	 * @param power  - power to calculate;
	 * @param matrix - the matrix to be calculated.
	 * @return returns the calculated matrix.
	 */
	public static double[][] power(int power, double[][] matrix) {
		double[][] result = matrix;

		for (int i = 1; i < power; i++) {
			result = multiply(result, matrix);
		}

		return result;
	}

	/**
	 * Sums two matrices.
	 *
	 * @param matrix        - any double matrix.
	 * @param anotherMatrix - any other double matrix.
	 * @return returns the calculated matrix.
	 */
	public static double[][] sum(double[][] matrix, double[][] anotherMatrix) {
		double[][] result = new double[matrix.length][matrix[0].length];

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result[i][j] = matrix[i][j] + anotherMatrix[i][j];
			}
		}

		return result;
	}

	/**
	 * Creates a matrix with all entries equal to one another.
	 *
	 * @param constant - any constant.
	 * @param size     - size of the matrix.
	 * @return returns a double array.
	 */
	public static double[][] createUniformMatrix(double constant, int size) {
		double[][] result = new double[size][size];

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				result[i][j] = constant;
			}
		}

		return result;
	}

	/**
	 * Calculates the highest eigenvalue.
	 *
	 * @param eigenInformation - Matrix array object of la4j library.
	 * @return returns the highest eigenvalue.
	 */
	public static double[] getHighestEigenvalue(Matrix[] eigenInformation) {
		double[] information = new double[2];
		double[][] eigenValues = eigenInformation[1].toDenseMatrix().toArray();
		double highest = Double.MIN_VALUE, index = -1d;

		for (int i = 0; i < eigenValues.length; i++) {
			double value = eigenValues[i][i];

			if (value != 0 && value > highest) {
				highest = value;
				index = i;
			}
		}

		information[0] = highest;
		information[1] = index;

		return information;
	}

	/**
	 * Gets a double array with the eigenvector.
	 *
	 * @param eigenInformation - Matrix object array from La4j library.
	 * @param index            - the index of the vector.
	 * @param size             - size of the matrix.
	 * @return returns the eigenvector.
	 */
	public static double[] getEigenvector(Matrix[] eigenInformation, int index, int size) {
		double[] vector = new double[size];
		double[][] eigenVectors = eigenInformation[0].toDenseMatrix().toArray();

		for (int i = 0; i < size; i++) {
			vector[i] = eigenVectors[i][index];
		}

		return vector;
	}

	/**
	 * This method normalizes a vector.
	 * 
	 * @param vector - vector.
	 * @return returns the normalized vector.
	 */
	public static double[] normalizeVector(double[] vector) {
		double[] result = vector;

		double normValue = norm(vector);

		for (int coordinate = 0; coordinate < vector.length; coordinate++) {
			result[coordinate] = vector[coordinate] / normValue;
		}

		return result;
	}

	/**
	 * This method normalizes a vector.
	 * 
	 * @param vector - column vector.
	 * @param column - column of the vector.
	 * @return returns the normalized vector.
	 */
	public static double[][] normalizeVector(double[][] vector, int column) {
		double[][] result = new double[vector.length][1];

		double normValue = norm(vector, column);

		for (int coordinate = 0; coordinate < vector.length; coordinate++) {
			result[coordinate][0] = vector[coordinate][column] / normValue;
		}

		return result;
	}

	/**
	 * This method gets the norm of a vector.
	 * 
	 * @param vector - vector.
	 * @return returns the norm of the vector.
	 */
	private static double norm(double[] vector) {
		double sumOfSquares = 0;

		for (int coordinate = 0; coordinate < vector.length; coordinate++) {
			sumOfSquares += Math.pow(vector[coordinate], 2);
		}

		return Math.sqrt(sumOfSquares);
	}

	/**
	 * This method gets the norm of a column vector.
	 * 
	 * @param vector - column vector.
	 * @param column - column of the vector.
	 * @return returns the norm of the column vector.
	 */
	private static double norm(double[][] vector, int column) {
		double sumOfSquares = 0;

		for (int coordinate = 0; coordinate < vector.length; coordinate++) {
			sumOfSquares += Math.pow(vector[coordinate][column], 2);
		}

		return Math.sqrt(sumOfSquares);
	}

}

package bitsplease.lapr.networkanalysis.util;

import bitsplease.lapr.networkanalysis.config.Configuration;
import bitsplease.lapr.networkanalysis.config.Mode;
import bitsplease.lapr.networkanalysis.metrics.Metrics;
import bitsplease.lapr.networkanalysis.network.DegreeType;
import bitsplease.lapr.networkanalysis.network.NetworkType;

import java.util.Formatter;

public class OutputHelper {

    /**
     * This method prints the node information.
     *
     * @param formatter   - formatter object.
     * @param nodes       - node information array.
     * @param networkType - network type.
     */
    public static void printNodes(Formatter formatter, String[][] nodes, NetworkType networkType) {
        formatter.format("%nNós da rede%n%n");

        if (networkType.equals(NetworkType.ORIENTED)) {
            formatter.format("%-7s%-25s%-18s%-15s%-30s%n", "ID", "Media", "Tipo de media", "Tipo", "URL");

            for (int i = 0; i < nodes.length; i++) {
                formatter.format("%-7s%-25s%-18s%-15s%-30s%n", nodes[i][Configuration.FIELD_NODE_ID],
                        nodes[i][Configuration.FIELD_NODE_MEDIA], nodes[i][Configuration.FIELD_NODE_MEDIA_TYPE],
                        nodes[i][Configuration.FIELD_NODE_TYPE_LABEL], nodes[i][Configuration.FIELD_NODE_URL]);
            }
        } else {
            formatter.format("%-7s%-25s%-18s%-15s%n", "ID", "Media", "Tipo de media", "Tipo");

            for (int i = 0; i < nodes.length; i++) {
                formatter.format("%-7s%-25s%-18s%-15s%n", nodes[i][Configuration.FIELD_NODE_ID],
                        nodes[i][Configuration.FIELD_NODE_MEDIA], nodes[i][Configuration.FIELD_NODE_MEDIA_TYPE],
                        nodes[i][Configuration.FIELD_NODE_TYPE_LABEL]);
            }
        }
    }

    /**
     * This method prints the branches of the network.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - the adjacency matrix related to the network.
     */
    public static void printBranches(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix) {
        formatter.format("%nRamos da rede%n%n");

        printMatrix(formatter, nodes, adjacencyMatrix);
    }

    /**
     * This method prints the node degree for every node in the network.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @param openWebsite     - whether to open the website or not.
     */
    public static void printNodesDegree(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix, boolean openWebsite) {
        formatter.format("%nGrau dos nós%n%n");

        int highestIndex = 0, highestValue = -1;

        for (int i = 0; i < nodes.length; i++) {
            int nodeDegree = Metrics.getNodeDegree(i, adjacencyMatrix);

            formatter.format("Nó %s (%s): %d%n", nodes[i][Configuration.FIELD_NODE_MEDIA],
                    nodes[i][Configuration.FIELD_NODE_ID], nodeDegree);

            if (openWebsite) {
                if (nodeDegree > highestValue) {
                    highestIndex = i;
                    highestValue = nodeDegree;
                }
            }
        }

        if (openWebsite) {
            WebHelper.openWebsite(nodes[highestIndex][Configuration.FIELD_NODE_URL]);
        }
    }

    /**
     * This method prints the average node degree.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     */
    public static void printAverageDegree(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix) {
        formatter.format("%nGrau médio: %.2f%n", Metrics.getAverageDegree(nodes, adjacencyMatrix));
    }

    /**
     * This method prints the density of a network.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     */
    public static void printDensity(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix) {
        formatter.format("%nDensidade: %.2f%n", Metrics.getDensity(nodes, adjacencyMatrix));
    }

    /**
     * This method prints the powers of the adjacency matrix.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @param power           - power value.
     */
    public static void printPowerMatrices(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix,
                                          int power) {
        formatter.format("%nPotências da matriz das adjacências:%n");

        for (int i = 0; i < power; i++) {
            formatter.format("%nMatriz de potência %d%n%n", i + 1);

            printMatrix(formatter, nodes, AlgebraHelper.power(i + 1, adjacencyMatrix));
        }
    }

    /**
     * This method prints the degree of a node.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @param degreeType      - degree type.
     * @param openWebsite     - whether to open the website or not.
     */
    public static void printNodesDegreeOriented(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix,
                                                DegreeType degreeType, boolean openWebsite) {
        formatter.format(
                degreeType.equals(DegreeType.IN) ? "%nGrau de entrada dos nós%n%n" : "%nGrau de saída dos nós%n%n");

        int highestIndex = 0, highestValue = -1;

        if (degreeType.equals(DegreeType.IN)) {
            for (int i = 0; i < nodes.length; i++) {
                int nodeDegree = Metrics.getNodeDegreeIn(i, adjacencyMatrix);

                formatter.format("Nó %s (%s): %d%n", nodes[i][Configuration.FIELD_NODE_MEDIA],
                        nodes[i][Configuration.FIELD_NODE_ID], nodeDegree);

                if (openWebsite) {
                    if (nodeDegree > highestValue) {
                        highestIndex = i;
                        highestValue = nodeDegree;
                    }
                }
            }
        } else {
            for (int i = 0; i < nodes.length; i++) {
                int nodeDegree = Metrics.getNodeDegreeOut(i, adjacencyMatrix);

                formatter.format("Nó %s (%s): %d%n", nodes[i][Configuration.FIELD_NODE_MEDIA],
                        nodes[i][Configuration.FIELD_NODE_ID], nodeDegree);

                if (openWebsite) {
                    if (nodeDegree > highestValue) {
                        highestIndex = i;
                        highestValue = nodeDegree;
                    }
                }
            }
        }

        if (openWebsite) {
            WebHelper.openWebsite(nodes[highestIndex][Configuration.FIELD_NODE_URL]);
        }
    }

    /**
     * This method prints the eigenvector centrality.
     *
     * @param formatter   - formatter object.
     * @param nodes       - node information array.
     * @param eigenvector - eigen vector.
     * @param openWebsite - whether to open the website or not.
     */
    public static void printEigenVectorCentrality(Formatter formatter, String[][] nodes, double[] eigenvector, boolean openWebsite) {
        formatter.format("%nCentralidade do vetor próprio%n%n");

        if (eigenvector != null) {
            int highestIndex = 0;
            double highestValue = -1;

            for (int i = 0; i < nodes.length; i++) {
                double coordinate = Math.abs(eigenvector[i]);

                formatter.format("Nó %s (%s): %.3f%n", nodes[i][Configuration.FIELD_NODE_MEDIA],
                        nodes[i][Configuration.FIELD_NODE_ID], coordinate);

                if (openWebsite) {
                    if (coordinate > highestValue) {
                        highestIndex = i;
                        highestValue = coordinate;
                    }
                }
            }

            if (openWebsite) {
                WebHelper.openWebsite(nodes[highestIndex][Configuration.FIELD_NODE_URL]);
            }
        } else {
            formatter.format("É impossível calcular a centralidade para esta matriz.%n");
        }
    }

    /**
     * This method prints the page rank vector.
     *
     * @param formatter       - formatter object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @param dampingFactor   - damping factor.
     * @param iterations      - number of iterations.
     * @param mode            - mode.
     * @param print           - whether to print the iterations or not.
     * @param openWebsite     - whether to open the website or not.
     */
    public static void printPageRankVector(Formatter formatter, String[][] nodes, double[][] adjacencyMatrix,
                                           double dampingFactor, int iterations, Mode mode, boolean print, boolean openWebsite) {
        double[][] eigenvector;

        if (iterations > 0) {
            formatter.format("%nPage rank com %d iterações e com damping factor de %.3f:%n%n", iterations, dampingFactor);
            eigenvector = Metrics.getPageRankVector(adjacencyMatrix, iterations, dampingFactor, nodes.length, print, formatter);
        } else {
            formatter.format("%nPage rank vetor próprio com damping factor de %.3f:%n%n", dampingFactor);
            eigenvector = Metrics.getPageRankVector(adjacencyMatrix, dampingFactor, nodes.length);
        }

        if (eigenvector != null) {
            int highestIndex = 0;
            double highestValue = -1;

            for (int i = 0; i < nodes.length; i++) {
                double coordinate = Math.abs(eigenvector[i][0]);

                formatter.format("Nó %s (%s): %.3f%n", nodes[i][Configuration.FIELD_NODE_MEDIA],
                        nodes[i][Configuration.FIELD_NODE_ID], coordinate);

                if (openWebsite) {
                    if (coordinate > highestValue) {
                        highestIndex = i;
                        highestValue = coordinate;
                    }
                }
            }

            if (openWebsite) {
                WebHelper.openWebsite(nodes[highestIndex][Configuration.FIELD_NODE_URL]);
            }
        } else {
            formatter.format("É impossível calcular a centralidade para esta matriz.%n");
        }
    }

    /**
     * This method prints a given bi-dimensional double matrix.
     *
     * @param formatter - formatter object.
     * @param nodes     - node information array.
     * @param matrix    - the node matrix related to the network.
     */
    public static void printMatrix(Formatter formatter, String[][] nodes, double[][] matrix) {
        int biggest = getBiggestLength(matrix, nodes.length);

        for (int i = -1; i < nodes.length; i++) {
            for (int j = -1; j < nodes.length; j++) {
                if (i < 0) {
                    if (j < 0) {
                        formatter.format("%-" + (biggest > 4 ? biggest : 4) + "s", "");
                    } else {
                        formatter.format("%-" + (biggest > 4 ? biggest : 4) + "s",
                                nodes[j][Configuration.FIELD_NODE_ID]);
                    }
                } else {
                    if (j < 0) {
                        formatter.format("%-" + (biggest > 4 ? biggest : 4) + "s",
                                nodes[i][Configuration.FIELD_NODE_ID]);
                    } else {
                        formatter.format("%-" + (biggest > 4 ? biggest : 4) + "d", Math.round(matrix[i][j]));
                    }
                }
            }

            formatter.format("%n");
        }
    }

    /**
     * Calculates the length of the biggest number.
     *
     * @param matrix - bi-dimensional double array.
     * @param size   - size of the matrix.
     * @return returns the length of the biggest number.
     */
    private static int getBiggestLength(double[][] matrix, int size) {
        double max = matrix[0][0];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                if (matrix[i][j] > max) {
                    max = matrix[i][j];
                }
            }
        }

        return String.valueOf(max).length();
    }
}
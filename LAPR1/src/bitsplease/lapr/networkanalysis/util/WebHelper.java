package bitsplease.lapr.networkanalysis.util;

import bitsplease.lapr.networkanalysis.config.Configuration;

import java.awt.*;
import java.net.URI;

public class WebHelper {

	/**
	 * Opens a website passed as an argument.
	 * 
	 * @param website - website to open.
	 */
	public static void openWebsite(String website) {
		try {
			Desktop desktop = java.awt.Desktop.getDesktop();
			URI oURL = new URI(website);
			desktop.browse(oURL);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Opens the highest rated websites.
	 * 
	 * @param eigenvector - eigen vector.
	 * @param nodes       - node information array.
	 */
	public static void openHighestRatedWebsites(double[] eigenvector, String[][] nodes) {
		double maxvalue = getHighestValue(eigenvector);

		if (eigenvector == null) return;
		
		for (int i = 0; i < eigenvector.length; i++) {
			if (eigenvector[i] == maxvalue) {
				openWebsite(nodes[i][Configuration.FIELD_NODE_URL]);
			}
		}
	}

	/**
	 * Opens the highest rated websites.
	 * 
	 * @param pageRankVector - page rank vector.
	 * @param nodes          - node information array.
	 */
	public static void openHighestRatedWebsites(double[][] pageRankVector, String[][] nodes) {
		double maxvalue = getHighestValue(pageRankVector);

		for (int i = 0; i < pageRankVector.length; i++) {
			if (pageRankVector[i][0] == maxvalue) {
				openWebsite(nodes[i][Configuration.FIELD_NODE_URL]);
			}
		}
	}

	/**
	 * Calculates the highest value of a vector.
	 * 
	 * @param vector - vector.
	 * @return returns the highest value.
	 */
	private static double getHighestValue(double[] vector) {
		for (int i = 0; i < vector.length; i++) {
			vector[i] = (double) Math.round(Math.abs(vector[i]) * 100000d) / 100000d;
		}

		double highestValue = Integer.MIN_VALUE;

		for (int i = 0; i < vector.length; i++) {
			if (vector[i] > highestValue) {
				highestValue = vector[i];
			}
		}
		return highestValue;
	}

	/**
	 * Calculates the highest value of a column vector.
	 * 
	 * @param vector - column vector.
	 * @return returns the highest value.
	 */
	private static double getHighestValue(double[][] vector) {
		for (int i = 0; i < vector.length; i++) {
			vector[i][0] = (double) Math.round(Math.abs(vector[i][0]) * 100000d) / 100000d;
		}

		double highestValue = Integer.MIN_VALUE;
		for (int i = 0; i < vector.length; i++) {
			if (vector[i][0] > highestValue) {
				highestValue = vector[i][0];
			}
		}
		return highestValue;
	}
}
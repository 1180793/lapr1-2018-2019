package bitsplease.lapr.networkanalysis.validation;

import bitsplease.lapr.networkanalysis.config.Configuration;
import bitsplease.lapr.networkanalysis.config.Messages;
import bitsplease.lapr.networkanalysis.config.Mode;
import bitsplease.lapr.networkanalysis.errorlog.ErrorLog;
import bitsplease.lapr.networkanalysis.errorlog.ErrorType;
import bitsplease.lapr.networkanalysis.file.FileManager;
import bitsplease.lapr.networkanalysis.network.NetworkType;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ValidationManager {

	/**
	 * Verifies if the given arguments are valid.
	 * 
	 * @param args - arguments of the runnable jar.
	 * @return returns whether the arguments are valid or not.
	 */
	public static boolean hasValidArguments(String[] args) {
		if (args.length > 0 && args[0].equalsIgnoreCase("--help")) {
			System.out.println(Messages.getHelpMessage());
			return false;
		} else if (args.length < 3) {
			System.out.println(Messages.NOT_ENOUGH_ARGUMENTS);
			return false;
		} else if (args.length != Configuration.FIELDS_MODE_N && args.length != Configuration.FIELDS_MODE_TK
				&& args.length != Configuration.FIELDS_MODE_TKD) {
			System.out.println(Messages.INVALID_ARGUMENTS_USAGE);
			return false;
		} else if (!args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-n")
				&& !args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-t")) {
			System.out.println(Messages.INVALID_ARGUMENTS_MODIFIER);
			return false;
		} else if (args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-n")
				&& args.length != Configuration.FIELDS_MODE_N) {
			System.out.println(Messages.INVALID_ARGUMENTS);
			return false;
		} else if (args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-t")
				&& (args.length != Configuration.FIELDS_MODE_TK && args.length != Configuration.FIELDS_MODE_TKD)) {
			System.out.println(Messages.INVALID_ARGUMENTS);
			return false;
		} else if (args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-t")
				&& !args[Configuration.FIELD_ARGS_TK].equalsIgnoreCase("-k")) {
			System.out.println(Messages.INVALID_ARGUMENTS_T_POWER);
			return false;
		} else if (args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-t")
				&& args[Configuration.FIELD_ARGS_TK].equalsIgnoreCase("-k")) {
			try {
				int value = Integer.parseInt(args[Configuration.FIELD_ARGS_TK_VALUE]);

				if (args.length == Configuration.FIELDS_MODE_TK) {
					if (value < 1) {
						System.out.println(Messages.INVALID_ARGUMENTS_T_POWERVALUE);
						return false;
					}
				} else {
					if (!args[Configuration.FIELD_ARGS_TKD].equalsIgnoreCase("-d")) {
						System.out.println(Messages.INVALID_ARGUMENTS);
						return false;
					}

					double dampingFactor = Double.parseDouble(args[Configuration.FIELD_ARGS_TKD_VALUE].replace(",", "."));

					if (!(dampingFactor >= 0 && dampingFactor <= 1)) {
						System.out.println(Messages.INVALID_DAMPING_FACTOR);
						return false;
					}
				}

			} catch (Exception e) {
				if (args.length == Configuration.FIELDS_MODE_TK) {
					System.out.println(Messages.INVALID_ARGUMENTS_T_POWERVALUE);
				} else {
					System.out.println(Messages.INVALID_DAMPING_FACTOR);
				}
				return false;
			}
		}

		return true;
	}

	/**
	 * Verifies if the given files on the arguments are valid.
	 *
	 * @param args - arguments of the runnable jar.
	 * @param mode - mode which the jar was executed.
	 * @return returns whether the files are valid or not.
	 */
	public static boolean hasValidFiles(String[] args, Mode mode) {
		if (FileManager.exists(args[args.length - 1]) && FileManager.exists(args[args.length - 2])) {
			return true;
		} else {
			System.out.println(Messages.INVALID_FILE);
			return false;
		}
	}

	/**
	 * Verifies if the network type of a network information file is valid.
	 * 
	 * @param args             - arguments of the runnable jar.
	 * @param branchesFilename - name of the branches file.
	 * @param mode             - mode which the jar was executed.
	 * @return returns whether the network type is valid or not.
	 */
	public static boolean hasValidNetworkType(String[] args, String branchesFilename, Mode mode) {
		Scanner scanner;

		try {
			scanner = new Scanner(new File(branchesFilename));
		} catch (FileNotFoundException e) {
			System.out.println(Messages.INVALID_FILE);
			return false;
		}

		boolean hasType = false;

		String type = "none";

		NetworkType expectedNetworkType = getExpectedNetworkType(args);

		while (scanner.hasNextLine() && !hasType) {
			String line = scanner.nextLine();

			if (!line.isEmpty()) {
				if (line.contains("networkType")) {
					String[] fields = line.split(":");

					if (fields.length == 2
							&& (fields[1].trim().equalsIgnoreCase("oriented") || fields[1].trim().equalsIgnoreCase("nonoriented"))) {
						hasType = true;
						type = fields[1].trim();
					}
				}
			}
		}

		scanner.close();

		if (mode.equals(Mode.INTERACTIVE)) {
			if (hasType) {
				return true;
			} else {
				ErrorLog.registerError(ErrorType.ERROR, Messages.INVALID_NETWORK_TYPE, Messages.INVALID_NETWORK_TYPE);
				return false;
			}
		} else {
			if (!hasType) {
				ErrorLog.registerError(ErrorType.ERROR, Messages.INVALID_NETWORK_TYPE, Messages.INVALID_NETWORK_TYPE);
				return false;
			} else if (hasType && expectedNetworkType.equals(NetworkType.valueOf(type.toUpperCase()))) {
				return true;
			} else {
				ErrorLog.registerError(ErrorType.ERROR, "Erro: " + Messages.INVALID_NETWORK_TYPE_MATCH.toLowerCase(),
						"O comando apresenta uma estrutura diferente da que é utilizada no tipo de network que está no ficheiro dos ramos. Verifique a sua validade.");
				return false;
			}
		}
	}

	/**
	 * Verifies if the file content is valid or not.
	 * 
	 * @param numberOfNodes   - number of nodes.
	 * @param nodes           - node information array.
	 * @param adjacencyMatrix - adjacency matrix.
	 * @return returns whether the file content is valid or not.
	 */
	public static boolean hasValidFileContent(int numberOfNodes, String[][] nodes, double[][] adjacencyMatrix) {
		if (numberOfNodes < 1) {
			System.out.println(Messages.INVALID_NODE_NUMBER);
			return false;
		} else if (numberOfNodes > Configuration.MAX_NODES) {
			System.out.println(Messages.MAX_NODES_EXCEEDED);
			return false;
		} else if (nodes == null) {
			System.out.println(Messages.INVALID_NODES_FILE);
			return false;
		} else if (adjacencyMatrix == null) {
			System.out.println(Messages.INVALID_BRANCHES_FILE);
			return false;
		}

		return true;
	}

	/**
	 * Checks the mode the program was executed.
	 * 
	 * @param args - arguments of the runnable jar.
	 * @return returns the mode which the program was executed.
	 */
	public static Mode getMode(String[] args) {
		if (args[Configuration.FIELD_ARGS_MODE].equalsIgnoreCase("-n")) {
			return Mode.INTERACTIVE;
		} else {
			return Mode.RECURSIVE;
		}
	}

	/**
	 * Verifies the expected network type according to the arguments. Only works for
	 * recursive mode.
	 * 
	 * @param args - arguments of the runnable jar.
	 * @return returns the expected network type.
	 */
	private static NetworkType getExpectedNetworkType(String[] args) {
		if (args.length == Configuration.FIELDS_MODE_TK) {
			return NetworkType.NONORIENTED;
		} else {
			return NetworkType.ORIENTED;
		}
	}
}
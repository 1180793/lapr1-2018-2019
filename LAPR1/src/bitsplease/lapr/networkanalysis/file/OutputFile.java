package bitsplease.lapr.networkanalysis.file;

import bitsplease.lapr.networkanalysis.config.Configuration;
import bitsplease.lapr.networkanalysis.config.Mode;
import bitsplease.lapr.networkanalysis.metrics.Metrics;
import bitsplease.lapr.networkanalysis.network.DegreeType;
import bitsplease.lapr.networkanalysis.network.NetworkType;
import bitsplease.lapr.networkanalysis.util.OutputHelper;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

public class OutputFile {

	/**
	 * Creates the output information file.
	 * 
	 * @param nodes            - node information array.
	 * @param adjacencyMatrix  - adjacency matrix.
	 * @param matrix           - matrix object from La4j library.
	 * @param networkType      - network type.
	 * @param mode             - mode which the program was executed.
	 * @param branchesFilename - name of the branches file.
	 * @param kValue           - power value of iterations value.
	 * @param dampingFactor    - damping factor.
	 */
	public static void createOutputFile(String[][] nodes, double[][] adjacencyMatrix, Matrix matrix,
			NetworkType networkType, Mode mode, String branchesFilename, int kValue, double dampingFactor) {
		String filename = getOutputFilename(branchesFilename);

		double start = System.nanoTime();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
		Date date = new Date();

		Formatter formatter;

		try {
			formatter = new Formatter(new File("output" + Configuration.FILE_NAME_SEPARATOR + filename
					+ Configuration.FILE_NAME_SEPARATOR + dateFormat.format(date) + ".txt"));
		} catch (FileNotFoundException e) {
			System.out.println("An unexpected error occurred.");
			return;
		}

		OutputHelper.printNodes(formatter, nodes, networkType);
		OutputHelper.printBranches(formatter, nodes, adjacencyMatrix);

		if (networkType.equals(NetworkType.NONORIENTED)) {
			OutputHelper.printNodesDegree(formatter, nodes, adjacencyMatrix, false);

			Matrix[] eigenInformation = new EigenDecompositor(matrix).decompose();
			double[] eigenvector = Metrics.getEigenvectorCentrality(eigenInformation, nodes.length);
			OutputHelper.printEigenVectorCentrality(formatter, nodes, eigenvector, false);

			OutputHelper.printAverageDegree(formatter, nodes, adjacencyMatrix);
			OutputHelper.printDensity(formatter, nodes, adjacencyMatrix);
			OutputHelper.printPowerMatrices(formatter, nodes, adjacencyMatrix, kValue);
		} else {
			OutputHelper.printNodesDegreeOriented(formatter, nodes, adjacencyMatrix, DegreeType.IN, false);
			OutputHelper.printNodesDegreeOriented(formatter, nodes, adjacencyMatrix, DegreeType.OUT, false);
			OutputHelper.printPageRankVector(formatter, nodes, adjacencyMatrix, dampingFactor, kValue, mode, true, false);
			OutputHelper.printPageRankVector(formatter, nodes, adjacencyMatrix, dampingFactor, -1, mode, true, false);
		}

		double timeElapsed = (System.nanoTime() - start) / 1000000000;

		System.out.println("Operação concluída em " + timeElapsed + " segundos.");

		formatter.close();
	}

	/**
	 * Gets the name of the output file.
	 *
	 * @param branchesFilename - name of the branches file.
	 * @return returns the name of the output file.
	 */
	private static String getOutputFilename(String branchesFilename) {
		String[] nameFields = branchesFilename.split(Configuration.FILE_NAME_SEPARATOR);

		if (nameFields.length == 3) {
			return nameFields[1];
		} else {
			return "invalid_name";
		}
	}
}
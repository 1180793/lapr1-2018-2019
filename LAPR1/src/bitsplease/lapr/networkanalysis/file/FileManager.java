package bitsplease.lapr.networkanalysis.file;

import bitsplease.lapr.networkanalysis.config.Configuration;
import bitsplease.lapr.networkanalysis.config.Messages;
import bitsplease.lapr.networkanalysis.errorlog.ErrorLog;
import bitsplease.lapr.networkanalysis.errorlog.ErrorType;
import bitsplease.lapr.networkanalysis.network.NetworkType;
import bitsplease.lapr.networkanalysis.network.NodeManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class FileManager {

    /**
     * Verifies if a given file exists.
     *
     * @param filename - name of the file to check.
     * @return returns whether the file exists or not.
     */
    public static boolean exists(String filename) {
        return new File(filename).exists();
    }

    /**
     * Calculates the number of nodes in the nodes file.
     *
     * @param nodesFilename - name of the nodes file.
     * @param networkType   - network type.
     * @return returns the number of nodes in the nodes file.
     */
    public static int getNumberOfNodes(String nodesFilename, NetworkType networkType) {
        Scanner scanner;

        try {
            scanner = new Scanner(new File(nodesFilename));
        } catch (FileNotFoundException e) {
            return -1;
        }

        int nodes = 0, lineNumber = 0;

        StringBuilder nodesIds = new StringBuilder();

        StringBuilder errorMessage = new StringBuilder();

        while (scanner.hasNextLine()) {
            lineNumber++;

            String line = scanner.nextLine();

            if (!line.isEmpty()) {

                if (line.charAt(0) == Configuration.ID_PREFIX) {
                    String[] fields = line.split(Configuration.FIELD_SEPARATOR);
                    if (fields.length == Configuration.FIELDS_NODES_FILE) {
                        if (!nodesIds.toString().contains(fields[Configuration.FIELD_NODE_ID])) {
                            nodes++;
                            nodesIds.append(fields[Configuration.FIELD_NODE_ID] + ";");
                        } else {
                            errorMessage.append("Informação inválida/duplicada na linha " + lineNumber + ";");
                        }
                    } else {
                        errorMessage.append("Informação inválida/duplicada na linha " + lineNumber + ";");
                    }
                }
            }
        }

        if (!errorMessage.toString().isEmpty()) {
            ErrorLog.registerError(ErrorType.WARNING,
                    "Aviso: existe informação inválida/duplicada no ficheiro dos nós. Verifique o registo relativo ao erro (crash-reports).",
                    errorMessage.substring(0, errorMessage.toString().length()).split(";"));
        }

        scanner.close();

        return nodes;
    }

    /**
     * Converts a file to a node information array.
     *
     * @param nodesFilename - name of the file to convert.
     * @param numberOfNodes - number of nodes to read.
     * @return returns the node information array.
     */
    public static String[][] convertFileToNodes(String nodesFilename, int numberOfNodes) {
        if (numberOfNodes < 1)
            return null;

        int numberOfFields = Configuration.FIELDS_NODES_FILE;

        String[][] nodes = new String[numberOfNodes][numberOfFields];

        Scanner scanner;

        try {
            scanner = new Scanner(new File(nodesFilename));
        } catch (FileNotFoundException e) {
            return null;
        }

        int nodeIndex = 0, lineNumber = 0;

        StringBuilder nodesIds = new StringBuilder();

        StringBuilder errorMessage = new StringBuilder();

        while (scanner.hasNextLine()) {
            lineNumber++;

            String line = scanner.nextLine();

            if (!line.isEmpty()) {
                String[] fields = line.split(Configuration.FIELD_SEPARATOR);

                if (line.charAt(0) == Configuration.ID_PREFIX && fields.length == numberOfFields) {
                    if (!nodesIds.toString().contains(fields[Configuration.FIELD_NODE_ID])) {
                        for (int field = 0; field < numberOfFields; field++) {
                            nodes[nodeIndex][field] = fields[field].trim();
                        }

                        nodeIndex++;

                        nodesIds.append(fields[Configuration.FIELD_NODE_ID] + ";");
                    }
                } else {
                    if (!line.contains("id,media,media.type,type.label")) {
                        errorMessage.append("Informação inválida/duplicada na linha " + lineNumber + " do ficheiro dos nós.;");
                    }
                }
            }
        }

        if (!errorMessage.toString().isEmpty()) {
            ErrorLog.registerError(ErrorType.WARNING,
                    "Aviso: existe informação inválida no ficheiro dos nós. Verifique o registo relativo ao erro (crash-reports).",
                    errorMessage.substring(0, errorMessage.toString().length()).split(";"));
        }

        scanner.close();

        return nodes;
    }

    /**
     * Converts a file to an adjacency matrix.
     *
     * @param branchesFilename - name of the file to convert.
     * @param nodes            - node information array.
     * @param numberOfNodes    - number of nodes to read.
     * @param networkType      - network type.
     * @return returns the node information array.
     */
    public static double[][] convertFileToAdjacencyMatrix(String branchesFilename, String[][] nodes, int numberOfNodes,
                                                          NetworkType networkType) {
        if (numberOfNodes < 1)
            return null;

        double[][] adjacencyMatrix = new double[numberOfNodes][numberOfNodes];

        Scanner scanner;

        try {
            scanner = new Scanner(new File(branchesFilename));
        } catch (FileNotFoundException e) {
            return null;
        }

        int lineNumber = 0;

        boolean hasDuplicatedInformation = false;

        StringBuilder errorMessage = new StringBuilder();

        while (scanner.hasNextLine()) {
            lineNumber++;

            String line = scanner.nextLine();

            if (!line.isEmpty()) {

                String[] fields = line.split(Configuration.FIELD_SEPARATOR);

                if (fields.length == Configuration.FIELDS_BRANCHES_FILE && line.charAt(0) == Configuration.ID_PREFIX) {
                    int fromindex = NodeManager.getNodeIndex(fields[Configuration.FIELD_BRANCH_FROM], nodes);
                    int toindex = NodeManager.getNodeIndex(fields[Configuration.FIELD_BRANCH_TO], nodes);

                    if (fromindex == -1 || toindex == -1) {
                        ErrorLog.registerError(ErrorType.ERROR,
                                "Erro: um dos nós ou ambos não existem. Verifique o registo relativo ao erro (crash-reports).",
                                "Nó(s) inválido(s) na linha " + lineNumber + ".");

                        adjacencyMatrix = null;

                        break;
                    } else if (toindex == fromindex && networkType.equals(NetworkType.NONORIENTED)) {
                        ErrorLog.registerError(ErrorType.ERROR,
                                "Erro: um nó não se pode ligar a si mesmo. Verifique o log relativo ao erro.",
                                "A ligação " + fields[Configuration.FIELD_BRANCH_FROM] + "-"
                                        + fields[Configuration.FIELD_BRANCH_TO] + " é inválida");

                        adjacencyMatrix = null;

                        break;
                    }

                    if (NodeManager.hasDuplicatedInformation(fromindex, toindex, adjacencyMatrix)
                            && networkType.equals(NetworkType.NONORIENTED)) {
                        if (!hasDuplicatedInformation) {
                            hasDuplicatedInformation = true;
                        }

                        errorMessage.append("Informação duplicada na ligação " + fields[Configuration.FIELD_BRANCH_FROM]
                                + "-" + fields[Configuration.FIELD_BRANCH_TO] + ";");
                    }

                    if (fields[2].trim().equalsIgnoreCase("1")) {
                        adjacencyMatrix[fromindex][toindex] = 1.0D;

                        if (networkType.equals(NetworkType.NONORIENTED)) {
                            adjacencyMatrix[toindex][fromindex] = 1.0D;
                        }
                    }
                } else {
                    if (!line.contains("networkType") && !line.equalsIgnoreCase("from,to,weight"))
                        errorMessage.append("Informação inválida na linha " + lineNumber + " do ficheiro dos ramos.;");
                }
            }
        }

        if (!errorMessage.toString().isEmpty() && adjacencyMatrix != null) {
            ErrorLog.registerError(ErrorType.WARNING,
                    "Aviso: existe informação inválida ou duplicada no ficheiro dos ramos. Verifique o registo relativo ao erro (crash-reports).",
                    errorMessage.toString().substring(0, errorMessage.toString().length()).split(";"));
        }

        scanner.close();

        return adjacencyMatrix;
    }

    /**
     * Gets the network type from the branches file.o
     *
     * @param branchesFilename - name of the branches file.
     * @return returns the network type.
     */
    public static NetworkType getNetworkType(String branchesFilename) {
        Scanner scanner;

        try {
            scanner = new Scanner(new File(branchesFilename));
        } catch (FileNotFoundException e) {
            System.out.println(Messages.INVALID_FILE);
            return null;
        }

        NetworkType networkType = null;

        while (scanner.hasNextLine() && networkType == null) {
            String line = scanner.nextLine().trim();

            if (!line.isEmpty()) {
                if (line.contains("networkType")) {
                    String[] fields = line.split(":");

                    if (fields.length == 2) {
                        networkType = NetworkType.valueOf(fields[1].toUpperCase());
                    }
                }
            }
        }

        scanner.close();

        return networkType;
    }
}
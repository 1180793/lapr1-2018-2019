package bitsplease.lapr.networkanalysis.metrics;

import bitsplease.lapr.networkanalysis.network.NodeManager;
import bitsplease.lapr.networkanalysis.util.AlgebraHelper;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

import java.util.Formatter;

public class Metrics {

    /**
     * Calculates the degree of a node.
     *
     * @param id              - id of the node.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @return return the degree of a node.
     */
    public static int getNodeDegree(String id, String[][] nodes, double[][] adjacencyMatrix) {
        int index = NodeManager.getNodeIndex(id, nodes);

        int nodeDegree = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            nodeDegree += adjacencyMatrix[index][i];
        }

        return nodeDegree;
    }

    /**
     * Calculates the degree of a node.
     *
     * @param index           - index of the node.
     * @param adjacencyMatrix - adjacency matrix.
     * @return return the degree of a node.
     */
    public static int getNodeDegree(int index, double[][] adjacencyMatrix) {
        int nodeDegree = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            nodeDegree += adjacencyMatrix[index][i];
        }

        return nodeDegree;
    }

    /**
     * Calculates the in degree of a node.
     *
     * @param index           - index of the node.
     * @param adjacencyMatrix - adjacency matrix.
     * @return returns the in degree of a node.
     */
    public static int getNodeDegreeIn(int index, double[][] adjacencyMatrix) {
        int nodeInDegree = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            nodeInDegree += adjacencyMatrix[i][index];
        }

        return nodeInDegree;

    }

    /**
     * Calculates the out degree of a node.
     *
     * @param index           - index of the node.
     * @param adjacencyMatrix - adjacency matrix.
     * @return returns the out degree of a node.
     */
    public static int getNodeDegreeOut(int index, double[][] adjacencyMatrix) {
        int nodeInDegree = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            nodeInDegree += adjacencyMatrix[index][i];
        }

        return nodeInDegree;

    }

    /**
     * Calculates the average degree.
     *
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @return returns the average degree.
     */
    public static double getAverageDegree(String[][] nodes, double[][] adjacencyMatrix) {
        double sum = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            sum += getNodeDegree(i, adjacencyMatrix);
        }

        return sum / adjacencyMatrix.length;
    }

    /**
     * Calculates the density.
     *
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @return returns the density.
     */
    public static double getDensity(String[][] nodes, double[][] adjacencyMatrix) {
        double branches = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            branches += getNodeDegree(i, adjacencyMatrix);
        }

        branches /= 2;

        return branches / (adjacencyMatrix.length * (adjacencyMatrix.length - 1) / 2D);
    }

    /**
     * Calculates the power of a matrix.
     *
     * @param matrix        - the matrix to calculate.
     * @param power         - the power to calculate.
     * @param numberOfNodes - number of nodes.
     * @return returns the resulting matrix.
     */
    public static double[][] getPowersOfAdjacencyMatrix(double[][] matrix, int power, int numberOfNodes) {
        return AlgebraHelper.power(power, matrix);
    }

    /**
     * Calculates the eigenvector centrality.
     *
     * @param eigenInformation - Matrix object array of la4j library.
     * @param size             - size of the matrix.
     * @return returns the eigenvector centrality.
     */
    public static double[] getEigenvectorCentrality(Matrix[] eigenInformation, int size) {
        double[] highestEigenvalue = AlgebraHelper.getHighestEigenvalue(eigenInformation);

        if (highestEigenvalue[0] > 0.01) {
            double[] eigenvector = AlgebraHelper.getEigenvector(eigenInformation, (int) highestEigenvalue[1], size);

            return AlgebraHelper.normalizeVector(eigenvector);
        } else {
            return null;
        }
    }

    /**
     * Calculates the page rank vector with an iterative method.
     *
     * @param adjacencyMatrix - the node matrix.
     * @param iteractions     - number of iterations.
     * @param dampingFactor   - damping factor.
     * @param size            - number of nodes.
     * @return returns the page rank vector.
     */
    public static double[][] getPageRankVector(double[][] adjacencyMatrix, int iteractions, double dampingFactor,
                                               int size, boolean print, Formatter formatter) {
        double[][] vec = new double[size][1];
        double[][] pagerankMatrix = MetricsHelper.getPageRankMatrix(adjacencyMatrix, dampingFactor, size);

        for (int j = 0; j < size; j++) {
            vec[j][0] = 1D / size;
        }

        if (print) {
            formatter.format("Iteração 0: [");
            for (int i = 0; i < size; i++){
                formatter.format(i < size - 1 ? "%.3f, " : "%.3f]", vec[i][0]);
            }
        }

        for (int j = 0; j < iteractions; j++) {
            double[][] aux = vec;

            vec = AlgebraHelper.multiply(pagerankMatrix, aux);

            if (print) {
                formatter.format("%nIteração %d: [", j + 1);

                for (int i = 0; i < size; i++) {
                    formatter.format(i < size - 1 ? "%.3f, " : "%.3f]", vec[i][0]);
                }
            }
        }

        if (print){
            formatter.format("%n%n");
        }

        return AlgebraHelper.normalizeVector(vec, 0);
    }

    /**
     * Calculates the page rank vector with the eigen vector.
     *
     * @param adjacencyMatrix - adjacency matrix.
     * @param dampingFactor   - damping factor.
     * @param size            - size of the matrix.
     * @return returns the page rank vector.
     */
    public static double[][] getPageRankVector(double[][] adjacencyMatrix, double dampingFactor, int size) {
        double[][] transformedMatrix = MetricsHelper.getPageRankMatrix(adjacencyMatrix, dampingFactor, size);
        Matrix[] eigenInformation = new EigenDecompositor(new Basic2DMatrix(transformedMatrix)).decompose();
        double[] highestEigenvalue = AlgebraHelper.getHighestEigenvalue(eigenInformation);

        if (highestEigenvalue[0] > 0.01) {
            double[] vec = AlgebraHelper.getEigenvector(eigenInformation, (int) highestEigenvalue[1], size);

            vec = AlgebraHelper.normalizeVector(vec);

            double[][] pagerankVector = new double[size][1];

            for (int i = 0; i < size; i++) {
                pagerankVector[i][0] = vec[i];
            }

            return pagerankVector;
        } else {
            return null;
        }
    }
}
package bitsplease.lapr.networkanalysis.metrics;

import bitsplease.lapr.networkanalysis.util.AlgebraHelper;

public class MetricsHelper {

    /**
     * Calculates the page rank matrix.
     *
     * @param adjacencyMatrix - adjacency matrix.
     * @param dampingFactor   - damping factor.
     * @param size            - size of the matrix.
     * @return returns the page rank matrix.
     */
    public static double[][] getPageRankMatrix(double[][] adjacencyMatrix, double dampingFactor, int size) {
        double[][] matrixA = AlgebraHelper.multiply(getTransformedMatrix(adjacencyMatrix), dampingFactor);
        double[][] matrixB = AlgebraHelper.createUniformMatrix(((1 - dampingFactor) / size), size);

        return AlgebraHelper.sum(matrixA, matrixB);
    }

    /**
     * Get auxiliary matrix for page rank.
     *
     * @param adjancencyMatrix - the adjacency matrix.
     * @return returns the resulting matrix.
     */
    private static double[][] getTransformedMatrix(double[][] adjancencyMatrix) {
        double[][] resultmatrix = new double[adjancencyMatrix.length][adjancencyMatrix[0].length];

        for (int i = 0; i < resultmatrix.length; i++) {
            for (int j = 0; j < resultmatrix[i].length; j++) {
                if (adjancencyMatrix[j][i] == 1) {
                    resultmatrix[i][j] = ((double) 1 / (double) Metrics.getNodeDegreeOut(j, adjancencyMatrix));
                } else if (Metrics.getNodeDegreeOut(j, adjancencyMatrix) == 0) {
                    resultmatrix[i][j] = ((double) 1 / (double) adjancencyMatrix.length);
                } else {
                    resultmatrix[i][j] = 0;
                }
            }
        }

        return resultmatrix;
    }
}
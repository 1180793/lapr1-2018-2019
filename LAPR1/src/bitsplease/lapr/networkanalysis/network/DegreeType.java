package bitsplease.lapr.networkanalysis.network;

public enum DegreeType {
	IN, OUT
}

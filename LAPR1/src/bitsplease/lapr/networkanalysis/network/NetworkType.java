package bitsplease.lapr.networkanalysis.network;

public enum NetworkType {
	ORIENTED, NONORIENTED
}

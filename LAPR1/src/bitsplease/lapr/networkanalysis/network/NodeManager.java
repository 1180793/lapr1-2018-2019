package bitsplease.lapr.networkanalysis.network;

import bitsplease.lapr.networkanalysis.config.Configuration;

public class NodeManager {

    /**
     * Searches for the index of the node id.
     *
     * @param id    - id of the node.
     * @param nodes - node information array.
     * @return returns the index of the node.
     */
    public static int getNodeIndex(String id, String[][] nodes) {
        int index = -1;

        for (int i = 0; i < nodes.length; i++) {
            if (nodes[i][Configuration.FIELD_NODE_ID].equalsIgnoreCase(id)) {
                return i;
            }
        }

        return index;
    }

    /**
     * Searches for the index of the node id.
     *
     * @param id            - id of the node.
     * @param nodes         - node information array.
     * @param numberOfNodes - number of nodes.
     * @return returns the index of the node.
     */
    public static int getNodeIndex(String id, String[][] nodes, int numberOfNodes) {
        int index = -1;

        for (int i = 0; i < numberOfNodes; i++) {
            if (nodes[i][Configuration.FIELD_NODE_ID].equalsIgnoreCase(id)) {
                return i;
            }
        }

        return index;
    }

    /**
     * Checks if the information is duplicated.
     *
     * @param from   - an index.
     * @param to     - another index.
     * @param matrix - the matrix to check.
     * @return returns whether the information is duplicated or not.
     */
    public static boolean hasDuplicatedInformation(int from, int to, double[][] matrix) {
        return matrix[from][to] == 1D || matrix[to][from] == 1D;
    }

}
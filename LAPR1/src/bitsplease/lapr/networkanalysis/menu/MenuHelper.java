package bitsplease.lapr.networkanalysis.menu;

import bitsplease.lapr.networkanalysis.config.Messages;
import bitsplease.lapr.networkanalysis.config.Mode;
import bitsplease.lapr.networkanalysis.metrics.Metrics;
import bitsplease.lapr.networkanalysis.util.OutputHelper;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;

import java.util.Formatter;
import java.util.Scanner;

public class MenuHelper {

    /**
     * This method prints the powers of the adjacency matrix.
     *
     * @param formatter       - formatter object.
     * @param scanner         - scanner object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     */
    static void printPowersOfAdjacencyMatrix(Formatter formatter, Scanner scanner, String[][] nodes,
                                             double[][] adjacencyMatrix) {
        formatter.format("%nIntroduza a potência máxima a calcular: ");

        String line = scanner.nextLine();

        try {
            int power = Integer.parseInt(line);

            if (power > 0) {
                OutputHelper.printPowerMatrices(formatter, nodes, adjacencyMatrix, power);
            } else {
                formatter.format("%n%s%n", Messages.INVALID_POWERVALUE);
            }
        } catch (Exception e) {
            formatter.format("%n%s%n", Messages.INVALID_POWERVALUE);
        }
    }

    /**
     * This method prints the page rank vector.
     *
     * @param formatter       - formatter object.
     * @param scanner         - scanner object.
     * @param nodes           - node information array.
     * @param adjacencyMatrix - adjacency matrix.
     * @param mode            - mode.
     * @param openWebsite     - whether to open the website or not.
     */
    static void printPagerankVector(Formatter formatter, Scanner scanner, String[][] nodes,
                                    double[][] adjacencyMatrix, Mode mode, boolean openWebsite) {
        formatter.format("%nIntroduza o damping factor: ");

        String line = scanner.nextLine();

        try {
            double factor = Double.parseDouble(line.replace(",", "."));

            while (!(factor > 0 && factor <= 1)) {
                formatter.format("%n%s%s", Messages.INVALID_DAMPING_FACTOR, " Introduza o damping factor: ");

                factor = Double.parseDouble(scanner.nextLine());
            }

            formatter.format("%nMétodo de cálculo:%n1 - Método iterativo%n2 - Vetor próprio%n%nOpção: ");

            line = scanner.nextLine().replace(",", ".");

            if (line.isEmpty())
                line = "z";

            while (line.charAt(0) != '1' && line.charAt(0) != '2') {
                formatter.format("%n%s", "Método inválido. Introdução a opção: ");

                line = scanner.nextLine();

                if (line.isEmpty())
                    line = "z";
            }

            if (line.charAt(0) == '1') {
                formatter.format("%nIntroduza o número de iterações: ");

                line = scanner.nextLine();

                try {
                    int iteractions = Integer.parseInt(line);

                    while (iteractions <= 0) {
                        formatter.format("%n%s", "Número de iterações inválido. Introduza novamente: ");

                        line = scanner.nextLine();

                        iteractions = Integer.parseInt(line);
                    }

                    OutputHelper.printPageRankVector(formatter, nodes, adjacencyMatrix, factor, iteractions, mode, true, openWebsite);
                } catch (Exception e) {
                    formatter.format("%nNúmero de iterações inválido, tem que ser um número inteiro.%n");
                }
            } else {
                OutputHelper.printPageRankVector(formatter, nodes, adjacencyMatrix, factor, -1, mode, false, openWebsite);
            }

        } catch (Exception e) {
            formatter.format("%n%s%n", Messages.INVALID_DAMPING_FACTOR);
        }
    }

    /**
     * This method prints the eigenvector centrality.
     *
     * @param formatter        - formatter object.
     * @param nodes            - node information array.
     * @param matrix           - matrix object from La4j library.
     * @param eigeninformation - eigen information matrix array.
     * @param openWebsite      - whether to open the website or not.
     * @return eigen information matrix array.
     */
    static Matrix[] printEigenvectorCentrality(Formatter formatter, String[][] nodes, Matrix matrix,
                                               Matrix[] eigeninformation, boolean openWebsite) {
        if (eigeninformation == null) {
            eigeninformation = new EigenDecompositor(matrix).decompose();
        }

        double[] eigenvector = Metrics.getEigenvectorCentrality(eigeninformation, nodes.length);

        OutputHelper.printEigenVectorCentrality(formatter, nodes, eigenvector, openWebsite);

        return eigeninformation;
    }
}
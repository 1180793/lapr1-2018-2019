package bitsplease.lapr.networkanalysis.menu;

import bitsplease.lapr.networkanalysis.config.Messages;
import bitsplease.lapr.networkanalysis.config.Mode;
import bitsplease.lapr.networkanalysis.network.DegreeType;
import bitsplease.lapr.networkanalysis.network.NetworkType;
import bitsplease.lapr.networkanalysis.util.OutputHelper;
import org.la4j.Matrix;

import java.util.Formatter;
import java.util.Scanner;

public class Menu {

	/**
	 * This method opens the menu.
	 * 
	 * @param nodes           - node information array.
	 * @param adjacencyMatrix - adjacency matrix.
	 * @param matrix          - matrix object from La4j library.
	 * @param numberOfNodes   - number of nodes.
	 * @param networkType     - network type.
	 * @param mode            - mode which the program was executed.
	 */
	public static void open(String[][] nodes, double[][] adjacencyMatrix, Matrix matrix, int numberOfNodes,
			NetworkType networkType, Mode mode) {
		Formatter formatter = new Formatter(System.out);
		Scanner scanner = new Scanner(System.in);

		char op;

		Matrix[] eigenInformation = null;

		do {
			op = readOption(formatter, scanner, networkType);

			if (networkType.equals(NetworkType.ORIENTED)) {
				switch (op) {
				case 'A':
					OutputHelper.printNodes(formatter, nodes, networkType);
					pause(formatter, scanner);
					break;
				case 'B':
					OutputHelper.printBranches(formatter, nodes, adjacencyMatrix);
					pause(formatter, scanner);
					break;
				case 'C':
					OutputHelper.printNodesDegreeOriented(formatter, nodes, adjacencyMatrix, DegreeType.IN, true);
					pause(formatter, scanner);
					break;
				case 'D':
					OutputHelper.printNodesDegreeOriented(formatter, nodes, adjacencyMatrix, DegreeType.OUT, true);
					pause(formatter, scanner);
					break;
				case 'E':
					MenuHelper.printPagerankVector(formatter, scanner, nodes, adjacencyMatrix, mode, true);
					pause(formatter, scanner);
					break;
				case '0':
					break;
				default:
					op = invalidOption(formatter, scanner);
					break;
				}
			} else {
				switch (op) {
				case 'A':
					OutputHelper.printNodes(formatter, nodes, networkType);
					pause(formatter, scanner);
					break;
				case 'B':
					OutputHelper.printBranches(formatter, nodes, adjacencyMatrix);
					pause(formatter, scanner);
					break;
				case 'C':
					OutputHelper.printNodesDegree(formatter, nodes, adjacencyMatrix, true);
					pause(formatter, scanner);
					break;
				case 'D':
					MenuHelper.printEigenvectorCentrality(formatter, nodes, matrix, eigenInformation, true);
					pause(formatter, scanner);
					break;
				case 'E':
					OutputHelper.printAverageDegree(formatter, nodes, adjacencyMatrix);
					pause(formatter, scanner);
					break;
				case 'F':
					OutputHelper.printDensity(formatter, nodes, adjacencyMatrix);
					pause(formatter, scanner);
					break;
				case 'G':
					MenuHelper.printPowersOfAdjacencyMatrix(formatter, scanner, nodes, adjacencyMatrix);
					pause(formatter, scanner);
					break;
				case '0':
					break;
				default:
					op = invalidOption(formatter, scanner);
					break;
				}
			}

		} while (op != '0');

		scanner.close();
		formatter.close();
	}

	/**
	 * This method returns the menu content.
	 *
	 * @param networkType - network type.
	 * @return returns the menu content.
	 */
	private static String getMenuContent(NetworkType networkType) {
		String menuContent = "Menu - Análise de redes sociais\n\n";

		if (networkType.equals(NetworkType.ORIENTED)) {
			menuContent += "Visualizaçao\n";
			menuContent += "A - Visualização dos nós.\n";
			menuContent += "B - Visualização dos ramos.\n\n";

			menuContent += "Medidas ao nível dos nós\n";
			menuContent += "C - Grau de entrada dos nós.\n";
			menuContent += "D - Grau de saída dos nós.\n";
			menuContent += "E - Page rank.\n\n";
		} else {
			menuContent += "Visualizaçao\n";
			menuContent += "A - Visualização dos nós.\n";
			menuContent += "B - Visualização dos ramos.\n\n";

			menuContent += "Medidas ao nível dos nós\n";
			menuContent += "C - Grau dos nós.\n";
			menuContent += "D - Centralidade do vetor próprio.\n\n";

			menuContent += "Medidas ao nível da rede\n";
			menuContent += "E - Grau médio.\n";
			menuContent += "F - Densidade.\n";
			menuContent += "G - Potências da matriz de adjacências.\n\n";
		}

		menuContent += "0 - Sair.\n\n";

		menuContent += "Opção: ";

		return menuContent;
	}

	/**
	 * This method let the user know that they typed an invalid option.
	 *
	 * @param formatter - formatter object.
	 * @param scanner   - scanner object.
	 * @return returns the option.
	 */
	private static char invalidOption(Formatter formatter, Scanner scanner) {
		formatter.format("%nOpção inválida. Deseja continuar? (Y/N) ");

		String line = scanner.nextLine();

		if (line.isEmpty()) {
			return '0';
		} else {
			if (line.equalsIgnoreCase("y") || line.equalsIgnoreCase("yes")) {
				formatter.format("%n");
				return 'Z';
			} else {
				return '0';
			}
		}
	}

	/**
	 * This method pauses the menu in order to let user see the last option they've
	 * choose.
	 *
	 * @param formatter - formatter object.
	 * @param scanner   - scanner object.
	 */
	private static void pause(Formatter formatter, Scanner scanner) {
		formatter.format("%s%n", Messages.MENU_CONTINUE);

		scanner.nextLine();
	}

	/**
	 * Asks the user to type an option.
	 * 
	 * @param formatter   - formatter object.
	 * @param scanner     - scanner object.
	 * @param networkType - network type.
	 * @return returns the option.
	 */
	private static char readOption(Formatter formatter, Scanner scanner, NetworkType networkType) {
		formatter.format(getMenuContent(networkType));

		String line = scanner.nextLine().toUpperCase();

		if (line.isEmpty()) {
			return 'Z';
		} else {
			return line.charAt(0);
		}
	}
}
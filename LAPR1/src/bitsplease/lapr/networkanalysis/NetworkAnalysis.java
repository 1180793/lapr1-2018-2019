package bitsplease.lapr.networkanalysis;

import bitsplease.lapr.networkanalysis.config.Configuration;
import bitsplease.lapr.networkanalysis.config.Mode;
import bitsplease.lapr.networkanalysis.file.FileManager;
import bitsplease.lapr.networkanalysis.file.OutputFile;
import bitsplease.lapr.networkanalysis.menu.Menu;
import bitsplease.lapr.networkanalysis.network.NetworkType;
import bitsplease.lapr.networkanalysis.validation.ValidationManager;
import org.la4j.Matrix;
import org.la4j.matrix.dense.Basic2DMatrix;

public class NetworkAnalysis {

	public static void main(String[] args) {
		String nodesFilename;
		String branchesFilename;

		Mode mode;
		NetworkType networkType;

		int numberOfNodes;

		String[][] nodes;
		Matrix matrix;
		double[][] adjacencyMatrix;

		if (!ValidationManager.hasValidArguments(args))
			return;

		mode = ValidationManager.getMode(args);

		if (!ValidationManager.hasValidFiles(args, mode))
			return;

		nodesFilename = args[args.length - 2];
		branchesFilename = args[args.length - 1];

		if (!ValidationManager.hasValidNetworkType(args, branchesFilename, mode))
			return;

		/*
		 * If the algorithm reaches this stage it is assured that the expected network
		 * type is the actual network type.
		 */

		networkType = FileManager.getNetworkType(branchesFilename);
		numberOfNodes = FileManager.getNumberOfNodes(nodesFilename, networkType);
		nodes = FileManager.convertFileToNodes(nodesFilename, numberOfNodes);
		adjacencyMatrix = FileManager.convertFileToAdjacencyMatrix(branchesFilename, nodes, numberOfNodes, networkType);

		if (!ValidationManager.hasValidFileContent(numberOfNodes, nodes, adjacencyMatrix)) {
			return;
		}

		matrix = new Basic2DMatrix(adjacencyMatrix);

		run(nodes, adjacencyMatrix, matrix, numberOfNodes, networkType, mode, branchesFilename, args);
	}

	/**
	 * Method to run the program.
	 * 
	 * @param nodes            - node information array.
	 * @param adjacencyMatrix  - adjacency matrix.
	 * @param matrix           - matrix object from La4j library.
	 * @param numberOfNodes    - number of nodes.
	 * @param networkType      - network type.
	 * @param mode             - mode which the program was executed.
	 * @param branchesFilename - name of the branches file.
	 * @param args             - arguments of the runnable jar file.
	 */
	private static void run(String[][] nodes, double[][] adjacencyMatrix, Matrix matrix, int numberOfNodes,
			NetworkType networkType, Mode mode, String branchesFilename, String[] args) {
		if (mode.equals(Mode.RECURSIVE)) {

			int kValue = Integer.parseInt(args[Configuration.FIELD_ARGS_TK_VALUE]);

			double dampingFactor;

			if (networkType.equals(NetworkType.ORIENTED)) {
				dampingFactor = Double.parseDouble(args[Configuration.FIELD_ARGS_TKD_VALUE].replace(",", "."));
			} else {
				dampingFactor = 0;
			}

			OutputFile.createOutputFile(nodes, adjacencyMatrix, matrix, networkType, mode, branchesFilename, kValue,
					dampingFactor);
		} else {
			Menu.open(nodes, adjacencyMatrix, matrix, numberOfNodes, networkType, mode);
		}
	}
}
package bitsplease.lapr.networkanalysis.errorlog;

import bitsplease.lapr.networkanalysis.config.Configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

public class ErrorLog {

	/**
	 * Creates a file with an error message.
	 * 
	 * @param type            - type of the error to register.
	 * @param terminalMessage - terminal message.
	 * @param errorMessage    - error message.
	 */
	public static void registerError(ErrorType type, String terminalMessage, String... errorMessage) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
		Date date = new Date();

		String filename = "crash-" + type.toString().toLowerCase() + "-" + dateFormat.format(date);

		if (!terminalMessage.isEmpty() && terminalMessage != null) {
			System.out.println(terminalMessage);
		}

		try {
			File file = new File(Configuration.LOG_FOLDER);

			file.mkdir();

			Formatter formatter = new Formatter(Configuration.LOG_FOLDER + filename + ".txt");

			for (String message : errorMessage) {
				formatter.format("%s%n", message);
			}

			formatter.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}

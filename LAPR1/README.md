# Network Analysis

This project was developed in the context of the curricular unit of LAPR1.

## Usage

Interactive mode:
```bash
java -jar networkanalysis.jar -n nodes_file.csv branches_file.csv
```

Recursive mode oriented network:
```bash
java -jar networkanalysis.jar -t -k iteraction_count -d damping_factor nodes_file.csv branches_file.csv
```
Recursive mode non-oriented network:
```bash
java -jar networkanalysis.jar -t -k power_value nodes_file.csv branches_file.csv
```